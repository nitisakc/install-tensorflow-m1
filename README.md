# install-tensorflow-m1



## Requirements
- [ ] macOS 11.0+
- [ ] Xcode Command Line Tools
- [ ] Python 3.8

## Update 2022-08
    - conda create --name tf python=3.9
    - conda activate tf
    - conda install -c apple tensorflow-deps
    - python -m pip install tensorflow-macos
    - python -m pip install tensorflow-metal
    - pip install mediapipe-silicon

#

## Install

1. Check macOS Version

    ```
    sw_vers -productVersion
    ```
##
2. Check Xcode Command Line Tools
    ```
    which xcrun
    ```
    ###
    2.1 If the result is not "/usr/bin/xcrun"
    ```
    xcode-select --install
    ```
##
3. Download [Miniforge](https://github.com/conda-forge/miniforge#miniforge3) for arm64 (Apple Silicon).
    OSX	arm64 (Apple Silicon)	[Miniforge3-MacOSX-arm64](https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-MacOSX-arm64.sh)
    ###
    3.1. Install Miniforge.
    ```
    chmod +x Miniforge3-MacOSX-arm64.sh
    ./Miniforge3-MacOSX-arm64.sh
    ```
    ###
    3.2. Reopen Terminal again and run the command to verify the python and pip path are at miniforge3.
    ```
    which python
    which pip
    ```
    ###
    3.3. Run python command in terminal and check Activity Monitor is running with the Apple Architecture.
##
4. Update Conda Version.
   ```
   conda update -n base -c defaults conda
   conda -V
   ```
##
5. Create environment from "env.yml".
    ```
    conda env create --file=env.yml
    ```
    5.1. Activate tf environment.
    ```
    conda activate tf
    ```
    ###
    5.2. Run python command in terminal and check Activity Monitor is running with the Apple Architecture and process name is "python3.8".
##
6. Install Tensorflow 2.4
   ```
   pip install --upgrade --force --no-dependencies https://github.com/apple/tensorflow_macos/releases/download/v0.1alpha2/tensorflow_addons_macos-0.1a2-cp38-cp38-macosx_11_0_arm64.whl
   ```
#
## GPU Test
1. Open Jupyter notebook
   ```
   jupyter notebook
   ```

2. Open and run "testGPU.ipynb" files.
    result:
     - 2.4.0.rc0
     - True
     - True
     - False

#
## IF Error
cannot import name 'builder' from 'google.protobuf.internal'
   - Install the latest protobuf
      ```
      pip install --upgrade protobuf
      ```
   - Copy builder.py from ~/miniforge3/envs/tf/lib/python3.8/site-packages/google/protobuf/internal to your computer
   - Install protobuf that compatible to your project
      ```
      pip install protobuf==3.19.4 --force
      ```
   - Copy builder.py to ~/miniforge3/envs/tf/lib/python3.8/site-packages/google/protobuf/internal